{-# LANGUAGE OverloadedStrings #-}
module Runner where

import           Control.Lens
import           Control.Monad                  ( void )
import           Data.Aeson                     ( toJSON
                                                , Value(..)
                                                )
import           Data.Aeson.Lens
import           Data.Set                       ( fromList
                                                , toList
                                                )
import           Data.Text                      ( pack
                                                , Text
                                                , unpack
                                                )
import           Development.Shake
import           Development.Shake.Forward
import           Development.Shake.FilePath
import           Slick
import           Transformations                ( filterPosts
                                                , formatPostDates
                                                , orgToHTML
                                                , withFormattedDate
                                                )
import           Types                          ( IndexInfo(..)
                                                , Post(..)
                                                , withSiteMeta
                                                )

outputFolder :: FilePath
outputFolder = "public/"

--- | Find and build all indices
buildIndices :: [Post] -> Action [()]
buildIndices ps = forP tags $ \tag -> buildIndex tag ps
 where
  tags = toList . fromList $ "index" : concat [ keywords post | post <- ps ]

-- | Build our atom feed
buildFeed :: [Post] -> Action ()
buildFeed ps = do
  feedT <- compileTemplate' "templates/atom.xml"
  let feedInfo = IndexInfo { posts = reverse ps }
      feedXML  = unpack $ substitute feedT (withSiteMeta $ toJSON feedInfo)
  writeFile' (outputFolder </> "atom.xml") feedXML

-- | Build a list of posts with a tag
buildIndex :: String -> [Post] -> Action ()
buildIndex tag ps = do
  indexT <- compileTemplate' "templates/index.html"
  let indexInfo = IndexInfo { posts = reverse (filterPosts tag ps) }
      indexHTML = unpack $ substitute indexT (withSiteMeta $ toJSON indexInfo)
  writeFile' (outputFolder </> tag ++ ".html") indexHTML

-- | Find and build all posts
buildPosts :: Action [Post]
buildPosts = do
  pPaths <- getDirectoryFiles "." ["posts//*.org"]
  forP pPaths buildPost

-- | Load a post, process metadata, write it to output, then return the post object
-- Detects changes to either post content or template
buildPost :: FilePath -> Action Post
buildPost srcPath = cacheAction ("build" :: Text, srcPath) $ do
  liftIO . putStrLn $ "Rebuilding post: " <> srcPath
  postContent <- readFile' srcPath
  -- load post content and metadata as JSON blob
  postData    <- orgToHTML . pack $ postContent
  let postUrl     = pack . dropDirectory1 $ srcPath -<.> "html"
      withPostUrl = _Object . at "url" ?~ String postUrl
  -- Add additional metadata we've been able to compute
  let fullPostData = withSiteMeta . withFormattedDate . withPostUrl $ postData
  template <- compileTemplate' "templates/post.html"
  writeFile' (outputFolder </> unpack postUrl) . unpack $ substitute
    template
    fullPostData
  convert fullPostData

-- | Copy all static files from the listed folders to their destination
copyStaticFiles :: Action ()
copyStaticFiles = do
  filepaths <- getDirectoryFiles
    "assets/"
    ["css//*.css", "fonts//*", "images//*", "js//*"]
  void $ forP filepaths $ \filepath ->
    copyFileChanged ("assets" </> filepath) (outputFolder </> filepath)

-- | Specific build rules for the Shake system
--   defines workflow to build the website
buildRules :: Action ()
buildRules = do
  allPosts <- buildPosts
  _        <- buildIndices allPosts
  _        <- buildFeed (formatPostDates "%A, %e %B %Y" "%FT00:00:00Z" allPosts)
  copyStaticFiles

main :: IO ()
main = do
  let shOpts = forwardOptions $ shakeOptions
        { shakeVerbosity  = Chatty
        , shakeLintInside = ["posts//*.org"]
        }
  shakeArgsForward shOpts buildRules
