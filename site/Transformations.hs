{-# LANGUAGE OverloadedStrings #-}

module Transformations
  ( filterPosts
  , formatPostDates
  , orgToHTML
  , withFormattedDate
  )
where

import           Data.Aeson
import qualified Data.HashMap.Strict           as HM
import           Data.Maybe                     ( fromJust
                                                , isNothing
                                                )
import           Data.Text                      ( Text
                                                , pack
                                                , unpack
                                                )
import           Data.Time                      ( Day
                                                , defaultTimeLocale
                                                , formatTime
                                                , parseTimeM
                                                )
import           Development.Shake              ( Action )
import           Slick.Pandoc                   ( defaultHtml5Options
                                                , loadUsing
                                                )
import           Text.Pandoc                    ( def
                                                , readOrg
                                                , writeHtml5String
                                                )
import           Types                          ( Post(..) )

-- | given a tag and a list of posts this will return those posts with the tag
-- Also provides a means to build the main index by passing "index" as the tag
filterPosts :: String -> [Post] -> [Post]
filterPosts tag posts
  | tag == "index" = posts
  | otherwise      = [ post | post <- posts, tag `elem` keywords post ]

-- | given a post this will format dates
formatPostDate :: String -> String -> Post -> Post
formatPostDate fromDateFormat toDateFormat p = Post
  { title    = title p
  , author   = author p
  , content  = content p
  , date     = formatDate (date p) fromDateFormat toDateFormat
  , url      = url p
  , image    = image p
  , keywords = keywords p
  }

-- | given a list of posts this will format dates
formatPostDates :: String -> String -> [Post] -> [Post]
formatPostDates fromDateFormat toDateFormat =
  map (formatPostDate fromDateFormat toDateFormat)

-- | Reformat string date from inputF to outputF
-- Return the string unchanged in parsing fails
formatDate :: String -> String -> String -> String
formatDate s inputF outputF
  | isNothing (parsedTime s inputF) = s
  | otherwise = formatTime defaultTimeLocale
                           outputF
                           (fromJust $ parsedTime s inputF)
  where parsedTime d i = parseTimeM True defaultTimeLocale i d :: Maybe Day

withFormattedDate :: Value -> Value
withFormattedDate (Object obj) = Object fields
  where fields = HM.filter (/= Null) (fmap withFormattedDate obj)
withFormattedDate (Array array) = Array array
withFormattedDate (String string) =
  String $ pack (formatDate (unpack string) "%Y-%-m-%-d" "%A, %e %B %Y")
withFormattedDate (Number number) = Number number
withFormattedDate (Bool   bool  ) = Bool bool
withFormattedDate Null            = Null

-- | Update the metadata on a Pandoc monad
-- To construct a Meta in the repl use `let met = Meta $ Map.fromList [("keywords", MetaInlines [Str "emacs, haskell"])]`
-- You can then construct a full Pandoc with
-- let t = "Text" :: Text
-- let para = Para [Str t]
-- let p = Pandoc met [para]
-- Function is based on https://stackoverflow.com/a/40024364
--splitKeywords :: Pandoc -> Pandoc 
--splitKeywords (Pandoc meta blocks) = Pandoc meta' blocks where
--  meta' = Meta (Map.insert "keywords" (MetaInlines [Str "emacs"]) (unMeta meta))

-- | Convert org text into a 'Value';
-- The 'Value'  has a "content" key containing rendered HTML
-- Metadata is assigned on the respective keys in the 'Value'
orgToHTML :: Text -> Action Value
orgToHTML = loadUsing (readOrg def) (writeHtml5String defaultHtml5Options)
