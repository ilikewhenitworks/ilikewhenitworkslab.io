{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}

module Types
  ( IndexInfo(..)
  , Post(..)
  , siteMeta
  , withSiteMeta
  )
where

import           Data.Aeson
import           Data.HashMap.Lazy              ( union )
import           Data.Time                      ( Day
                                                , defaultTimeLocale
                                                , getCurrentTime
                                                , parseTimeM
                                                )
import           Development.Shake.Classes      ( Binary )
import           GHC.Generics                   ( Generic )
import           System.IO.Unsafe               ( unsafePerformIO )

data SiteMeta =
    SiteMeta { siteAuthor    :: String
             , baseUrl       :: String
             , updated       :: String
             , siteTitle     :: String
             }
    deriving (Generic, Eq, Ord, Show, ToJSON)

-- | Data for the index page
data IndexInfo =
  IndexInfo
    { posts :: [Post]
    } deriving (Generic, Show, FromJSON, ToJSON)

-- | Data for a blog post
data Post =
    Post { title    :: String
         , author   :: [String]
         , content  :: String
         , date     :: String
         , url      :: String
         , image    :: Maybe String
         , keywords :: [String]
         }
    deriving (Generic, Eq, Show, FromJSON, ToJSON, Binary)

-- | Order posts by date
instance Ord Post where
  p1 `compare` p2 =
    (fetchDay (date p1) :: Maybe Day)
      `compare` (fetchDay (date p2) :: Maybe Day)
    where fetchDay = parseTimeM True defaultTimeLocale "%A, %e %B %Y"

siteMeta :: SiteMeta
siteMeta = SiteMeta { siteAuthor = "Garry Cairns"
                    , baseUrl    = "https://ilikewhenitworks.gitlab.io"
                    , updated    = unsafePerformIO (fmap show getCurrentTime)
                    , siteTitle  = "I like when it works"
                    }

withSiteMeta :: Value -> Value
withSiteMeta (Object obj) = Object $ union obj siteMetaObj
  where Object siteMetaObj = toJSON siteMeta
withSiteMeta _ = error "Only add site meta to objects"
