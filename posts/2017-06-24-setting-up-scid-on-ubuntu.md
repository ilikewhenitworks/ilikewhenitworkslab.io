---
title: Setting up SCID on Ubuntu
author: Garry Cairns
tags: chess, ubuntu
---
[SCID](http://scid.sourceforge.net/) is one of my favourite pieces of chess software. But if you install it from the Ubuntu 17.04 repositories you'll find some features don't quite work for you straight away. I'm going to show you how to get them running. In this short post we'll cover:

* installing scid from the repository;
* adding analysis engines; and
* setting things up so you can train and play against an engine.

I've assumed a new Ubuntu user throughout the post.

### Installing

Open a terminal with <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>A</kbd>. Type `sudo apt-get install scid` into it and enter your password when prompted. That will install SCID but not any chess engines for you to play against or help analyse your games, so you need to follow it up with `sudo apt-get install phalanx stockfish`. You need both of these engines because, while stockfish is stronger, phalanx is the one scid needs in order to play against you at lower strengths.

### Setting up engines

I've found setting up engines to be unintuitive so what follows is what worked for me. There may be better ways to do this. Open SCID and, from the analysis engine window (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>A</kbd>), click New. Complete the following fields for stockfish.

* Name: stockfish-toga
* Command: /usr/games/stockfish
* Directory: /home/your-username/.scid4.6
* UCI: checked

Adding `toga` to stockfish's name seemed to be important in allowing player vs engine games to work properly. For phalanx use:

* Name: phalanx
* Command: /usr/games/phalanx
* Directory: /home/your-username/.scid4.6
* UCI: unchecked

Here, unchecking UCI for phalanx is important for allowing player vs engine games.

### Playing against an engine

The above setup should let you play two types of games against your engines. Using the `Play` menu in SCID you can play against stockfish using the `Serious game` option, which will be very strong, or against phalanx using the `Tactical game` option, which will let you set the engine ELO.

### Quickly building up a database of games

You don't need scripting skills to do this, but they help. I put together a 2 million + database of master games in a couple of hours with some [Python](https://www.python.org/) scripts. For example, to download every pgn from [The Week in Chess](http://theweekinchess.com/) between weeks 960 and 1180 you can use:

    >>> import os
    >>> import urllib.request as ur
    >>> import zipfile
 
    >>> for count in range (960, 1180):
    ...     ur.urlretrieve("http://www.theweekinchess.com/zips/twic{}g.zip".format(count), "/path/to/save/your/files/twic{}.zip".format(count))

    >>> for f in os.listdir('/path/to/save/your/files'):
    ...     if f.endswith('zip'):
    ...         fn = '/path/to/save/your/files/' + f
    ...         with zipfile.ZipFile(fn) as zf:
    ...             zf.extractall(path='/path/to/save/your/files')

You can then import these through the drop down menu on the database tab in SCID. Once I've imported games I like to maintain the database by deleting twin games and removing any games where the players' ELOs are below 2200. You can filter for ELO using a header search (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>H</kbd>) and deletions happen in the maintenance window (<kbd>Ctrl</kbd>+<kbd>M</kbd>).

If you don't fancy learning any coding to do this then downloading manually will be fine, if slow. The Week in Chess and [PGN mentor](http://www.pgnmentor.com/files.html) are the best places I've found to get master games.

### Training with your favourite players

Once you've got a database set up you can use a nice feature in SCID to play as, or against, some of your favourite players. I use this to practice middlegames arising from openings I try to play. Set the board up into the position you want then search the board with <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>B</kbd>. This will list all games from your database that contain that position. Open a game that suits what you want to practice, for example a game where your favourite player won from the position on the board. Then use the `Play>Training>Review game` option and you will be able to play through the game, with SCID asking you each time the opponent makes a move what move you would make. SCID keeps a score of how often you moved like the real player and, when you didn't, tells you if your move was also good.

That should give you enough to get started training with SCID. Please [raise an issue](TODOs) if you're having any trouble with the steps given here, or if you have any corrections to offer.
