<!doctype html>
<html lang="en">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="I like when it works">
    <meta name="author" content="Garry Cairns">
    <link rel="canonical" href="https://ilikewhenitworks.gitlab.io">
    <title>How I (currently) start a Haskell project in NixOS—I like when it works</title>

    <link rel="stylesheet" href="/css/style.css" />

    <link rel="icon" type="image/png" href="/images/favicon.png">

  </head>

  <body>

    <header>
      <h1 class="logo">
        <a href="/" title="Index">ILikeWhenItWorks</a>
      </h1>
      <nav>
        <a href="/" title="Back to the home page">home</a>
        <a href="https://gitlab.com/garry-cairns" title="My projects">projects</a>
        <a href="/atom.xml" title="Subscribe with Atom" rel="subscribe-rss">feed</a>
      </nav>
    </header>

    <main role="main">
      <article>
        <h2>How I (currently) start a Haskell project in NixOS</h2>
        <section class="post-info">
        Posted on Sunday, 14 October 2018
        by Garry Cairns
        </section>
        <section class="tags">
        Tagged with: <a href="/haskell.html">haskell</a> <a href="/nixos.html">nixos</a> 
        </section>
        <p>Learning Haskell over a long period, due to extremely limited free time, has had one interesting side effect. In the years I've been investing an hour here and an hour there into learning the Haskell way, the Haskell way has itself changed numerous times. So when I started what will hopefully become my first non-trivial, non-framework-assisted Haskell program recently I wasn't too surprised that I didn't yet know the right way to start.</p>
<h2>Starting without stack</h2>
<p><a href="https://docs.haskellstack.org/en/stable/README/">Stack</a> is what prompted my first successful attempt to write any Haskell at all. Before it was released I struggled just to get a running compiler. It's a good tool and I suggest you use it when you're starting with the language.</p>
<p>But along the way I started running my home laptop on <a href="https://nixos.org/">NixOS</a>. I enjoy NixOS because I can recreate my system from <a href="https://gitlab.com/garry-cairns/nixos-config">a plain text configuration</a> whenever I get a new laptop, but I've had problems getting Stack projects running on it. Since I spend very little time learning Haskell and I wanted to get a project started rather than debugging infrastructure, Stack had to go.</p>
<h2>Plain old Cabal and Nix</h2>
<p>If you want to start a Haskell project in Nix you'll eventually find your way to <a href="https://github.com/Gabriel439/haskell-nix">Gabriel Gonzalez's monster guide to the subject</a>. What follows is not an update, replacement or amendment to that. It's only my distillation of the quick steps I found to get a new project started in the way I like them. I'm assuming you already have Cabal the tool installed (<code class="sourceCode bash"><span class="ex">nix-env</span> -iA nixos.cabal-install</code>) and cabal2nix (<code class="sourceCode bash"><span class="ex">nix-env</span> -iA nixos.cabal2nix</code>).</p>
<p>First I run <code class="sourceCode bash"><span class="ex">cabal</span> init</code> in the directory in which I want my code to live. The app I'm starting will be both an executable and a library so I answer cabal's questions accordingly. I like to write my code into a library and then have an executable being just one user of that library so the library isn't too tied to my implementation. To that end I create an <code class="sourceCode bash"><span class="ex">app</span></code> directory and move <code class="sourceCode bash"><span class="ex">Main.hs</span></code> in there. I also create a <code class="sourceCode bash"><span class="bu">test</span></code> directory and <code class="sourceCode bash"><span class="fu">touch</span> test/Spec.hs</code>. Finally I create a <code class="sourceCode bash"><span class="ex">nix</span></code> directory to store the project's nix configurations.</p>
<p>Before creating any nix configuration it's wise to edit the project cabal file a little. I expose any modules I expect to create in the library section, include expected build depends, and make projectName a build dep for app and test. The test part you'll likely need to add. It might look a little like the below.</p>
<div class="sourceCode" id="cb1"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb1-1"><a href="#cb1-1" aria-hidden="true"></a>test<span class="op">-</span>suite projectName<span class="op">-</span>test</span>
<span id="cb1-2"><a href="#cb1-2" aria-hidden="true"></a>  <span class="kw">type</span><span class="op">:</span>                exitcode<span class="op">-</span>stdio<span class="op">-</span><span class="fl">1.0</span></span>
<span id="cb1-3"><a href="#cb1-3" aria-hidden="true"></a>  hs<span class="op">-</span>source<span class="op">-</span>dirs<span class="op">:</span>      test</span>
<span id="cb1-4"><a href="#cb1-4" aria-hidden="true"></a>  main<span class="op">-</span>is<span class="op">:</span>             Spec.hs</span>
<span id="cb1-5"><a href="#cb1-5" aria-hidden="true"></a>  build<span class="op">-</span>depends<span class="op">:</span>       base</span>
<span id="cb1-6"><a href="#cb1-6" aria-hidden="true"></a>                     , projectName</span>
<span id="cb1-7"><a href="#cb1-7" aria-hidden="true"></a>                     , hspec</span>
<span id="cb1-8"><a href="#cb1-8" aria-hidden="true"></a>                     , <span class="dt">QuickCheck</span></span>
<span id="cb1-9"><a href="#cb1-9" aria-hidden="true"></a>  ghc<span class="op">-</span>options<span class="op">:</span>         <span class="op">-</span>threaded <span class="op">-</span>rtsopts <span class="op">-</span>with<span class="op">-</span>rtsopts<span class="op">=-</span><span class="dt">N</span></span>
<span id="cb1-10"><a href="#cb1-10" aria-hidden="true"></a>  default<span class="op">-</span>language<span class="op">:</span>    <span class="dt">Haskell2010</span></span></code></pre></div>
<p>Now we can write our nix configurations. First get <code class="sourceCode bash"><span class="ex">cabal2nix</span></code> to create your <code class="sourceCode bash"><span class="ex">default.nix</span></code> with <code class="sourceCode bash"><span class="ex">cabal2nix</span>  . <span class="op">&gt;</span> nix/default.nix</code>. You'll need to edit what it produces to point the <code class="sourceCode bash"><span class="ex">src</span></code> directory up one level since it normally expects to be at the same level, which I avoid because I think looks a bit untidy. There's likely a way to automate that but I haven't looked for one.</p>
<p>Now add a <code class="sourceCode bash"><span class="ex">nix/projectName.nix</span></code> file that looks like the below:</p>
<div class="sourceCode" id="cb2"><pre class="sourceCode nix"><code class="sourceCode bash"><span id="cb2-1"><a href="#cb2-1" aria-hidden="true"></a><span class="bu">let</span></span>
<span id="cb2-2"><a href="#cb2-2" aria-hidden="true"></a></span>
<span id="cb2-3"><a href="#cb2-3" aria-hidden="true"></a><span class="ex">pkgs</span> = import <span class="op">&lt;</span>nixpkgs<span class="op">&gt;</span> { };</span>
<span id="cb2-4"><a href="#cb2-4" aria-hidden="true"></a></span>
<span id="cb2-5"><a href="#cb2-5" aria-hidden="true"></a><span class="kw">in</span></span>
<span id="cb2-6"><a href="#cb2-6" aria-hidden="true"></a><span class="kw">{</span> <span class="ex">projectName</span> = pkgs.haskellPackages.callPackage ./default.nix { <span class="kw">}</span>;</span>
<span id="cb2-7"><a href="#cb2-7" aria-hidden="true"></a>}</span></code></pre></div>
<p>This should leave a structure like:</p>
<div class="sourceCode" id="cb3"><pre class="sourceCode bash"><code class="sourceCode bash"><span id="cb3-1"><a href="#cb3-1" aria-hidden="true"></a><span class="ex">parent_directory/</span></span>
<span id="cb3-2"><a href="#cb3-2" aria-hidden="true"></a>       <span class="ex">projectName.cabal</span></span>
<span id="cb3-3"><a href="#cb3-3" aria-hidden="true"></a>       <span class="ex">...</span></span>
<span id="cb3-4"><a href="#cb3-4" aria-hidden="true"></a> <span class="ex">app/</span></span>
<span id="cb3-5"><a href="#cb3-5" aria-hidden="true"></a>  <span class="ex">Main.hs</span></span>
<span id="cb3-6"><a href="#cb3-6" aria-hidden="true"></a>       <span class="ex">nix/</span></span>
<span id="cb3-7"><a href="#cb3-7" aria-hidden="true"></a>           <span class="ex">default.nix</span></span>
<span id="cb3-8"><a href="#cb3-8" aria-hidden="true"></a>           <span class="ex">projectName.nix</span></span>
<span id="cb3-9"><a href="#cb3-9" aria-hidden="true"></a> <span class="ex">src/</span></span>
<span id="cb3-10"><a href="#cb3-10" aria-hidden="true"></a>  <span class="ex">ProjectLib.hs</span></span>
<span id="cb3-11"><a href="#cb3-11" aria-hidden="true"></a> <span class="ex">test/</span></span>
<span id="cb3-12"><a href="#cb3-12" aria-hidden="true"></a>  <span class="ex">Spec.hs</span></span></code></pre></div>
<h2>The first build</h2>
<p>Build the project in Nix with <code class="sourceCode bash"><span class="ex">nix-build</span> nix/projectName.nix</code>. Now you can enter a fully set up command line environment with <code class="sourceCode bash"><span class="ex">nix-shell</span> --attr projectName.env nix/projectName.nix</code>. Once inside that shell run <code class="sourceCode bash"><span class="ex">cabal</span> new-build</code> any time you want to rebuild your project. Running emacs from the nix-shell will also ensure your emacs <code class="sourceCode bash"><span class="ex">haskell-mode</span></code> is using the right environment.</p>
<h2>Start writing a Spec</h2>
<p>Import your libraries and write an initial expected behviour. Remember if you define your own data types then they need to be exported in the lib along with their constructors in order to be usable in the test. For my nascent chess program my first spec looked like this:</p>
<div class="sourceCode" id="cb4"><pre class="sourceCode haskell"><code class="sourceCode haskell"><span id="cb4-1"><a href="#cb4-1" aria-hidden="true"></a><span class="ot">{-# LANGUAGE OverloadedStrings #-}</span></span>
<span id="cb4-2"><a href="#cb4-2" aria-hidden="true"></a></span>
<span id="cb4-3"><a href="#cb4-3" aria-hidden="true"></a><span class="kw">module</span> <span class="dt">Main</span> <span class="kw">where</span></span>
<span id="cb4-4"><a href="#cb4-4" aria-hidden="true"></a></span>
<span id="cb4-5"><a href="#cb4-5" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">ChessLib</span></span>
<span id="cb4-6"><a href="#cb4-6" aria-hidden="true"></a><span class="kw">import</span> <span class="dt">Test.Hspec</span></span>
<span id="cb4-7"><a href="#cb4-7" aria-hidden="true"></a></span>
<span id="cb4-8"><a href="#cb4-8" aria-hidden="true"></a><span class="ot">main ::</span> <span class="dt">IO</span> ()</span>
<span id="cb4-9"><a href="#cb4-9" aria-hidden="true"></a>main <span class="ot">=</span> hspec <span class="op">$</span></span>
<span id="cb4-10"><a href="#cb4-10" aria-hidden="true"></a>  describe <span class="st">&quot;newGame&quot;</span> <span class="op">$</span></span>
<span id="cb4-11"><a href="#cb4-11" aria-hidden="true"></a>    it <span class="st">&quot;sets up a Position in the tradition chess starting position&quot;</span> <span class="op">$</span></span>
<span id="cb4-12"><a href="#cb4-12" aria-hidden="true"></a>      newGame <span class="ot">`shouldBe`</span> <span class="dt">Start</span> <span class="st">&quot;rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1&quot;</span></span></code></pre></div>
<p>After watching that fail appropriately I'm ready to start the <a href="https://sites.google.com/site/agilepatterns/home/red-green-refactor">red, green, refactor cycle</a>. Once the spec is passing I make my first commit, write my next spec and, as far as I'm concerned, the project is officially underway.</p>
      </article>
    </main>

    <footer>
      <h2>About</h2>
      <p>I’m Garry Cairns. I’m a software engineer working in the finance industry. This is my personal site, where I typically write about whatever fun I’m having on personal projects.</p>
      <p>Content is pubished under <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.</p>
      <p>It works thanks to <a href="https://hackage.haskell.org/package/slick">λ Slick</a></p>
      <p>Source available on <a href="https://gitlab.com/ilikewhenitworks/ilikewhenitworks.gitlab.io">GitLab</a></p>
    </footer>

  </body>

</html>
