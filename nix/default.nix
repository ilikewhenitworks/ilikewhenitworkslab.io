{ mkDerivation, aeson, base, containers, lens, lens-aeson, pandoc
, shake, slick, stdenv, text, time, unordered-containers
}:
mkDerivation {
  pname = "ILikeWhenItWorks";
  version = "0.3.0.0";
  src = ../.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson base containers lens lens-aeson pandoc shake slick text time
    unordered-containers
  ];
  license = stdenv.lib.licenses.bsd3;
}
